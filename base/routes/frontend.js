const express = require("express");
const router = express.Router();
const path = require('path');
const fs = require('fs');

const articlesController = require("../controllers/articlesController");
const categoriesController = require("../controllers/categoriesController");

const mabase = ['articles', 'categories'];
const iterator = mabase.values();

for (const controller of iterator) {

    router.get(`/${controller}/liste`, require(`../controllers/${controller}Controller`).liste);
    router.get(`/${controller}/create`, require(`../controllers/${controller}Controller`).create);
    router.post(`/${controller}/store`, require(`../controllers/${controller}Controller`).store);
    router.post(`/${controller}/store/:id`, require(`../controllers/${controller}Controller`).upgrade);
    router.get(`/${controller}/update/:id`, require(`../controllers/${controller}Controller`).update);
    router.get(`/${controller}/remove/:id`, require(`../controllers/${controller}Controller`).remove);
    router.get(`/${controller}/show/:id`, require(`../controllers/${controller}Controller`).show);

}

router.get("/articles/excel", articlesController.excel);
router.get("/articles/pdf", articlesController.pdf);
router.post('/articles/mailer', articlesController.mailere);
router.get('/articles/contact', articlesController.mailing)
router.get("/articles/myexcell", articlesController.myexcell);


module.exports = router;