exports.excel = function(req, res) {
    knex.select('categories.title as macategorie', 'articles.*').from('categories').innerJoin('articles', 'categories.id', 'articles.categorie_id').then(function(articles) {

        const jsonCustomers = JSON.parse(JSON.stringify(articles));

        let workbook = new excel.Workbook();

        let worksheet = workbook.addWorksheet('Customers');

        var head = [
            'id', 'name'
        ];
        var key = [
            'id', 'email'
        ];
        worksheet.columns = [
            { header: 'Id', key: 'id', width: 10 },
            { header: 'Name', key: 'email', width: 30 },
        ];


        worksheet.addRows(jsonCustomers);
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + 'customer.xlsx');
        return workbook.xlsx.write(res)
            .then(function() {
                res.status(200).end();
            });


    });

    exports.monexcel = function(req, res, array1, array2) {

        var xl = require('excel4node');

        // Create a new instance of a Workbook class
        var wb = new xl.Workbook();

        // Add Worksheets to the workbook
        var ws = wb.addWorksheet('Sheet 1');
        var ws2 = wb.addWorksheet('Sheet 2');

        // Create a reusable style
        var style = wb.createStyle({
            font: {
                color: '#FF0800',
                size: 12,
            },
            numberFormat: '$#,##0.00; ($#,##0.00); -',
        });

        const iterator = mabase.values();

        for (const controller of iterator) {



        }
        // Set value of cell A1 to 100 as a number type styled with paramaters of style
        ws.cell(1, 1)
            .number(100)
            .style(style);

        for (let i = 0; i < array1.length; i++) {
            ws.cell(1, i + 1)
                .number(100)
                .style(style);

            array1[i];
        }
        // Set value of cell B1 to 200 as a number type styled with paramaters of style
        ws.cell(1, 2)
            .number(200)
            .style(style);


        // Set value of cell C1 to a formula styled with paramaters of style
        ws.cell(1, 3)
            .formula('A1 + B1')
            .style(style);

        // Set value of cell A2 to 'string' styled with paramaters of style
        ws.cell(2, 1)
            .string('string')
            .style(style);

        // Set value of cell A3 to true as a boolean type styled with paramaters of style but with an adjustment to the font size.
        ws.cell(3, 1)
            .bool(true)
            .style(style)
            .style({ font: { size: 14 } });

        wb.write('Excel.xlsx');


    }
}