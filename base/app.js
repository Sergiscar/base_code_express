const express = require('express'),
    bodyParser = require('body-parser'),
    OAuth2Server = require('oauth2-server'),
    Request = OAuth2Server.Request,
    Response = OAuth2Server.Response;
const PORT = process.env.PORT || 3000;
const knex = require('./knex/knex.js');
const path = require('path');
require("dotenv").config();
const app = express();
const crypto = require('crypto');
const logger = require("morgan");
const config = require('config');
const cookieParser = require("cookie-parser");
const sassMiddleware = require("node-sass-middleware");
const session = require("express-session");
const socket = require("socket.io");
const getHashedPassword = (password) => {
    const sha256 = crypto.createHash('sha256');
    const hash = sha256.update(password).digest('base64');
    return hash;
};
/*const jwt = require('jsonwebtoken');
const authTokens = {};
const generateAuthToken = () => {
    return crypto.randomBytes(30).toString('hex');
}*/
const articles = require("./routes/frontend");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'views'));
var engine = require('ejs-blocks');

app.engine('ejs', engine);
app.set('view engine', 'ejs');
app.set('view options', { layout: 'layout.ejs' });
app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

app.oauth = new OAuth2Server({
    model: require('./model.js'),
    accessTokenLifetime: 60 * 60,
    allowBearerTokensInQueryString: true
});




app.post('/create', require('./model').createUser);
app.delete('/delete/:id', require('./model').deleteUser);
app.put('/update/:id', require('./model').updateUser)

app.all('/oauth/token', function(req, res) {

    var request = new Request(req);
    var response = new Response(res);

    return app.oauth.token(request, response).then(function(token) {
        res.json(token);
    }).catch(function(err) {
        res.status(err.code || 500).json(err);
    });
});

app.get('/', authenticateRequest, function(req, res) {

    res.send('Congratulations, you are in a secret area!');
});

function obtainToken(req, res) {

    var request = new Request(req);
    var response = new Response(res);

    return app.oauth.token(request, response)
        .then(function(token) {

            res.json(token);
        }).catch(function(err) {

            res.status(err.code || 500).json(err);
        });
}

function authenticateRequest(req, res, next) {

    var request = new Request(req);
    var response = new Response(res);

    return app.oauth.authenticate(request, response)
        .then(function(token) {

            next();
        }).catch(function(err) {

            res.status(err.code || 500).json(err);
        });
}


//authentification
/*
app.get("/", (req, res) => res.render("auth/home"));
app.get('/register', (req, res) => {
    res.render('auth/register');
});
app.get('/login', (req, res) => {
    message = "";
    res.render('auth/login', { message: message });
});
app.get('/protected', (req, res) => {
    if (req.user) {
        res.render('protected');
    } else {
        res.render('auth/login', {
            message: 'Please login to continue',
            messageClass: 'alert-danger'
        });
    }
});


app.post('/register', (req, res) => {
    const { email, firstName, lastName, password, confirmPassword } = req.body;

    // Check if the password and confirm password fields match
    if (password === confirmPassword) {
        knex('users').where({ email: email }).then(function(users) {
            if (users.length > 0) {
                res.render('auth/register', {
                    message: 'User already registered.',
                    messageClass: 'alert-danger'

                });
                // Check if user with the same email is also registered


            }
            if (users.length == 0) {

                const hashedPassword = getHashedPassword(password);
                knex('users').insert({ id: 1, firstName: firstName, email: email, lastName: lastName, password: hashedPassword, confirmPassword: hashedPassword })
                    .then(function(articles) {
                        res.render('auth/login', {
                            message: 'Registration Complete. Please login to continue.',
                            messageClass: 'alert-success'
                        });

                    });

                // Store user into the database if you are using one

            }

        });

    } else {
        res.render('auth/register', {
            message: 'Password does not match.',
            messageClass: 'alert-danger'
        });
    }
});

app.post('/login', (req, res) => {
    const { email, password } = req.body;
    const hashedPassword = getHashedPassword(password);
    knex('users').where({ email: email, password: hashedPassword }).then(function(users) {
        if (users.length > 0) {
            const authToken = generateAuthToken();

            // Store authentication token
            // [authToken] = users;
            var token = jwt.sign(users, config.secret, {
                expiresIn: 10000 // in seconds
            });

            // Setting the auth token in cookies
            res.cookie('AuthToken', authTokens);

            // Redirect user to the protected page
            res.redirect('/protected');

        }
        if (users.length == 0) {
            res.render('auth/login', {
                message: 'Invalid username or password',
                messageClass: 'alert-danger'
            });
        }
    });

});*/

app.use("/frontend", articles);

const auth = require("./routes/auth");
app.use("/auth", auth);

const server = app.listen(PORT, () => {
    console.log(`Listening on port: ${PORT}`);
});

const io = socket(server);

io.on("connection", function(socket) {
    console.log("Made socket connection");
});
console.log("Made socket connection");