var client = require('../knexfile').development;
var knex = require('knex')(client);
var fs = require('fs');
const excel = require('exceljs');
var engine = require('ejs-blocks');
var email = require("nodemailer");
const xl = require('excel4node');
const wb = new xl.Workbook();
const ws = wb.addWorksheet('Worksheet Name');
var ejs = require('ejs');
const xl = require('excel4node');
var pdf = require('html-pdf');
var nodemailer = require('nodemailer');
var compiled = ejs.compile(fs.readFileSync('./views/articles/liste.html', 'utf8'));
const headingColumnNames = [
    "Name",
    "Email",
    "Mobile",
]
const headingtype = [
    "String",
    "String",
    "Number",
    "String",
    "String",
    "Number",
    "Number",
    "String"
]
const listechamp = [
    "macategorie",
    "id",
    "title",
    "email"
]


exports.liste = function(req, res) {
    try {

        knex.select('categories.title as macategorie', 'articles.*').from('categories').innerJoin('articles', 'categories.id', 'articles.categorie_id').then(function(articles) {
            return res.render('articles/index', { data: articles })
        });


    } catch (err) {
        console.error(err);

    }
};

exports.create = function(req, res) {
    return res.render("articles/create");
}

exports.mailing = function(req, res) {
    return res.render("articles/contact");
}

exports.store = function(req, res) {

    knex('articles').insert({ title: req.body.title, email: req.body.email, note: req.body.note, categorie_id: req.body.categorie_id })
        .then(function(articles) {
            return res.redirect("/frontend/articles/liste");
        });

}

exports.update = function(req, res) {
    const myid = req.params.id;
    knex('articles').where({ id: myid }).first().
    then(function(articles) {
        return res.render("articles/update", { article: articles });
    });


}

exports.upgrade = function(req, res) {
    const myid = req.params.id;

    knex('articles').where({ id: myid }).first().update({ title: req.body.title, email: req.body.email, note: req.body.note, categorie_id: req.body.categorie_id })
        .then(function(articles) {
            return res.redirect("/frontend/articles/liste")
        });



}

exports.remove = function(req, res) {
    const myid = req.params.id;
    knex('articles').
    where({ id: myid }).first()
        .del().then(function(articles) {
            return res.redirect("/frontend/articles/liste");
        });


}

exports.show = function(req, res) {
    const myid = req.params.id;
    knex.select('categories.title as macategorie', 'articles.*').from('categories').where('articles.id', myid)
        .innerJoin('articles', 'categories.id', 'articles.categorie_id')
        .first().then(function(articles) {
            return res.render("articles/show", { article: articles });
        });

}

exports.excel = function(req, res) {
    knex.select('categories.title as macategorie', 'articles.*').from('categories').innerJoin('articles', 'categories.id', 'articles.categorie_id').then(function(articles) {

        const jsonCustomers = JSON.parse(JSON.stringify(articles));

        let workbook = new excel.Workbook();

        let worksheet = workbook.addWorksheet('Customers');


        worksheet.columns = [
            { header: 'Id', key: 'id', width: 10 },
            { header: 'Name', key: 'email', width: 30 },
        ];


        worksheet.addRows(jsonCustomers);
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + 'customer.xlsx');
        return workbook.xlsx.write(res)
            .then(function() {
                res.status(200).end();
            });


    });
}

exports.pdf = async function(req, res) {
    let a = await knex.select('categories.title as macategorie', 'articles.*').from('categories').innerJoin('articles', 'categories.id', 'articles.categorie_id');
    var html = compiled({ title: 'EJS', text: 'Hello, World!', data: a });

    console.log(a);
    pdf.create(html).toFile('./result.pdf', () => {
        console.log('pdf done')
    })
}


exports.mailer = function(req, res) {


    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'dsgiscar@gmail.com',
            pass: 'sgdoleffo@gmail.com',
        }
    });
    console.log('created');
    transporter.sendMail({
        from: 'dsgiscar@gmail.com',
        to: 'serginodoleffogmail.com',
        subject: 'hello world!',
        text: 'hello world!',
    });
}
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'serginodoleffo@gmail.com',
        pass: 'dsgiscar@gmail.com'
    }
});


exports.mailere = function(req, res) {
    var name = req.body.name;
    var message = req.body.message;
    var email = req.body.email;
    var mailOptions = {
        from: 'serginodoleffo@gmail.com',
        to: 'dsgiscar@gmail.com',
        subject: 'Sending Email using Node.js',
        text: 'That was easy!'
    };
    var mailOptions = {
        from: 'serginodoleffo@gmail.com',
        to: 'dsgiscar@gmail.com',
        subject: email,
        html: '<h2>Nouveau message</h2><p> ' + message + '</p> <p><h3>' + name + '</h3></p>',
    };
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
            return res.redirect("/frontend/articles/contact");

        }
    });
}
exports.myexcell = async function(req, res) {
    let a = await knex.select('categories.title as macategorie', 'articles.*').from('categories').innerJoin('articles', 'categories.id', 'articles.categorie_id');
    const jsonCustomers = JSON.parse(JSON.stringify(a));

    let headingColumnIndex = 1;
    headingColumnNames.forEach(heading => {
        ws.cell(1, headingColumnIndex++)
            .string(heading)
    });

    let rowIndexe = 1;
    let rowIndex = 2;
    jsonCustomers.forEach(record => {
        let columnIndex = 1;
        Object.keys(record).forEach(columnName => {

            if (listechamp.includes(columnName)) {
                console.log("Tout va bien")
                if (headingtype[columnIndex] === "String") {
                    ws.cell(rowIndex, columnIndex++)
                        .string(record[columnName])
                } else if (headingtype[columnIndex] === "Number") {
                    ws.cell(rowIndex, columnIndex++)
                        .number(record[columnName])

                }
            }
        });
        rowIndex++;
    });
    wb.write('TeacherData.xlsx');
 
}

