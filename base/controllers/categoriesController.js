var client = require('../knexfile').development;
var knex = require('knex')(client);

exports.liste = function(req, res) {
    try {

        knex.select().from('categories').then(function(articles) {

            return res.render('categories/index', { data: articles })
        });


    } catch (err) {
        console.error(err);

    }
};

exports.create = function(req, res) {
    return res.render("categories/create");
}

exports.store = function(req, res) {

    knex('categories').insert({ title: req.body.title, description: req.body.description })
        .then(function(articles) {
            return res.redirect("/frontend/categories/liste");
        });
}

exports.update = function(req, res) {
    const myid = req.params.id;
    knex('categories').where({ id: myid }).first().
    then(function(articles) {
        return res.render("categories/update", { article: articles });
    });


}

exports.upgrade = function(req, res) {
    const myid = req.params.id;
    knex('categories').where({ id: myid }).first().update({ title: req.body.title, description: req.body.description })
        .then(function(articles) {
            return res.redirect("/frontend/categories/liste")
        });

}

exports.remove = function(req, res) {
    const myid = req.params.id;
    knex('categories').
    where({ id: myid }).first()
        .del().then(function(articles) {
            return res.redirect("/frontend/categories/liste");
        });


}

exports.show = function(req, res) {
    const myid = req.params.id;
    knex('categories')
        .where({ id: myid }).first().then(function(articles) {
            return res.render("categories/show", { article: articles });
        });

}